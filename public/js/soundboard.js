$(document).ready(function () {
  // We will have access to a sounds array variable thanks to soundboard.ejs.

  // Initialize the Buzz sounds.
  // TODO: only permit one sound to play at a time.
  sounds = sounds.map(function (s) {
    return new buzz.sound('/sound/' + s._id, {
      preload: true,
      autoplay: false,
      loop: false,
      webAudioApi: true
    });
  });

  // Handler to capture the correct value of i.
  var generate_handler = function (i) {
    return function (e) {
      e.preventDefault();
      sounds[i].play();
    };
  };

  // Start rigging them up to each respective play button.
  for (var i = 0; i < sounds.length; i++) {
    $('#play' + i).click(generate_handler(i));
  }
});