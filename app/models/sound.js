var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

// TODO: add all references sounds
var SoundSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  fid: {
    type: ObjectId,
    ref: 'File'
  },
  user: {
    type: ObjectId,
    ref: 'User'
  },
  soundboard: {
    type: ObjectId,
    ref: 'Soundboard'
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    required: true,
    default: Date.now
  }
});

SoundSchema.statics = {
  load: function (id, cb) {
    this.findOne({_id: id})
    .exec(cb);
  }
};

mongoose.model('Sound', SoundSchema);
