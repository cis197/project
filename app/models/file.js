var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var FileSchema = new Schema({
  buffer: {
    type: Buffer,
    required: true
  }
});

FileSchema.statics = {
  load: function (id, cb) {
    this.findOne({_id: id})
    .exec(cb);
  }
};

mongoose.model('File', FileSchema);
