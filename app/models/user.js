var mongoose = require('mongoose');
var passportLocalMongoose = require('passport-local-mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  soundboards: [{
    type: ObjectId,
    ref: 'Soundboard'
  }],
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    required: true,
    default: Date.now
  }
});

UserSchema.statics = {
  load: function (id, cb) {
    this.findOne({_id: id})
    .populate('soundboards')
    .exec(cb);
  }
};

UserSchema.plugin(passportLocalMongoose, {
  'usernameLowerCase': true
});

mongoose.model('User', UserSchema);
