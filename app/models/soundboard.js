var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

// TODO: add all references sounds
var SoundboardSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  user: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
  sounds: [{
    type: ObjectId,
    ref: 'Sound'
  }],
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    required: true,
    default: Date.now
  }
});

SoundboardSchema.statics = {
  load: function (id, cb) {
    this.findOne({_id: id})
    .populate('sounds')
    .exec(cb);
  }
};

mongoose.model('Soundboard', SoundboardSchema);
