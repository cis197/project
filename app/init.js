var path = require('path'),
    config = require('../config/config'),
    express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    session = require('express-session'),
    passport = require('passport'),
    flash = require('connect-flash'),
    ejs = require('ejs'),
    layouts = require('express-ejs-layouts'),
    expressValidator = require('express-validator');

var db = require('../config/db');
var User = db.model('User');

module.exports = function (app) {
  app.disable('x-powered-by');
  // Render the EJS view engine.
  app.use(layouts);
  app.set('views', path.join(config.appRoot, 'views'));
  app.engine('.ejs', ejs.__express);
  app.set('view engine', 'ejs');
  app.set('layout', 'layouts/layout');

  app.set('port', process.env.PORT || 3000);

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(expressValidator());
  app.use(cookieParser(config.cookieSecret));
  app.use(session({
    secret: config.cookieSecret,
    cookie: {maxAge: 1000 * 60 * 60},
    saveUninitialized: true,
    resave: true
  }));
  app.use(flash());

  app.use(passport.initialize());
  app.use(passport.session());

  passport.use(User.createStrategy());
  passport.serializeUser(User.serializeUser());
  passport.deserializeUser(User.deserializeUser());

  app.use(function(req, res, next) {
    res.locals.successFlashes = req.flash('success');
    res.locals.errorFlashes = req.flash('error');
    res.locals.authUser = req.user;

    next();
  });

  // Hook up routes to URLs.
  app.use('/static', express.static(config.root + '/public'));
  app.use('/', require('./routes/pages'));
  app.use('/soundboard', require('./routes/soundboard'));
  app.use('/sound', require('./routes/sound'));
  app.use('/user', require('./routes/user'));

  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  /*eslint-disable no-unused-vars */
  app.use(function(err, req, res, next) {
    if (err.status !== 404) {
      console.error(err);
    }
    err.status = err.status || 500;
    res.status(err.status).render('error', {
      layout: false,
      error: {
        message: err.message,
        status: err.status,
        stack: app.get('env') === 'development' ? err.stack : ''
      }
    });
  });
  /*eslint-enable no-unused-vars */
};
