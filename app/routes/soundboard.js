var express = require('express');

var soundboard = require('../controllers/soundboard'),
    auth = require('../middlewares/auth.js');

var router = express.Router();

router.get('/new', auth.requireLogin, soundboard.new);
router.post('/new', auth.requireLogin, soundboard.create);
router.get('/:id', soundboard.get);
router.get('/:id/edit', auth.requireLogin, soundboard.edit);
router.post('/:id/swap', auth.requireLogin, soundboard.swap);

module.exports = router;
