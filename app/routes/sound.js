var express = require('express'),
    multer = require('multer');

var sound = require('../controllers/sound'),
    auth = require('../middlewares/auth.js');

var router = express.Router();

router.get('/:id', sound.get);

router.post('/new', auth.requireLogin, multer({
    limits: {
      // These multer settings don't seem to work, unfortunately.
      // They're instead checked for in the controller.
      'fieldSize': 100 * 1000, // 100 KB
      'fileSize': 100 * 1000
    },
    inMemory: true}),
  sound.create);

router.post('/:id/delete', auth.requireLogin, sound.delete);

module.exports = router;
