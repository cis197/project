var express = require('express');

var user = require('../controllers/user'),
    auth = require('../middlewares/auth.js');

var router = express.Router();

router.get('/:id', user.soundboards);
router.get('/my/soundboards', auth.requireLogin, user.self);

module.exports = router;
