var express = require('express');
var pages = require('../controllers/pages');
var user = require('../controllers/user');

var router = express.Router();

router.get('/', pages.index);
router.get('/login', pages.login);
router.get('/logout', pages.logout);

router.post('/login', user.login);
router.post('/register', user.register);

module.exports = router;
