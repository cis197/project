var db = require('../../config/db');

var Soundboard = db.model('Soundboard');

exports.get = function (req, res, next) {
  var id = req.params.id;
  Soundboard.load(id, function (err, board) {
    if (err) {
      return next(err);
    }
    return res.render('soundboard', {
      layout: 'layouts/soundboard',
      soundboard: board
    });
  });
};

exports.new = function(req, res) {
  return res.render('new_soundboard');
};

exports.create = function(req, res, next) {
  var post = req.body;
  var name = post.name;
  var desc = post.description;
  var user = req.user;

  req.checkBody('name', 'Invalid soundboard name.').notEmpty();
  req.checkBody('description', 'Invalid description.').notEmpty();

  var errors = req.validationErrors();
  if (errors) {
    req.flash('error', errors[0].msg);
    return res.redirect('new');
  }

  var boardObj = new Soundboard({
    name: name,
    description: desc,
    user: user._id
  });

  // TODO: check if the board name is unique to a user.
  boardObj.save(function (errBoard, board) {
    if (errBoard) {
      return next(errBoard);
    }
    // Finally, update the user's collection.
    user.soundboards.push(board);
    user.save(function (errUser) {
      if (errUser) {
        return next(errUser);
      }
      return res.redirect('/soundboard/' + board._id + '/edit');
    });
  });
};

exports.edit = function(req, res, next) {
  var boardId = req.params.id;
  var user = req.user;

  Soundboard.load(boardId, function (err, board) {
    if (err) {
      return next(err);
    }
    if (board.user.toString() !== user._id.toString()) {
      console.log(user._id.toString() + ' tried to edit a soundboard owned ' +
        'by ' + board.user.toString() + '.');
      req.flash('error', 'You do not own that soundboard!');
      return res.redirect('/soundboard/' + boardId);
    }
    return res.render('edit_soundboard', {
      layout: 'layouts/soundboard',
      soundboard: board
    });
  });
};

exports.swap = function(req, res, next) {
  var boardId = req.params.id;
  var user = req.user;

  Soundboard.load(boardId, function (err, board) {
    if (err) {
      return next(err);
    }
    if (board.user.toString() !== user._id.toString()) {
      console.log(user._id.toString() + ' tried to edit a soundboard owned ' +
        'by ' + board.user.toString() + '.');
      req.flash('error', 'You do not own that soundboard!');
      return res.redirect('/soundboard/' + boardId);
    }

    // Do the actual swapping here.
    var i = toInt(req.body.i);
    var j = toInt(req.body.j);
    if (i < 0 || j < 0 || i >= board.sounds.length || j >= board.sounds.length) {
      req.flash('error', 'Invalid indices for sound swap.');
      return res.redirect('/soundboard/' + boardId + '/edit');
    }
    // TODO: implement. Requires us to change the DB schema.
    return res.redirect('/soundboard/' + boardId + '/edit');
  });
};

var toInt = function (s) {
  var n = ~~Number(s);
  return String(n) === s ? n : -1;
};
