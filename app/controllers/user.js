var db = require('../../config/db');
var passport = require('passport');

var User = db.model('User');

exports.login = passport.authenticate('local', {
  successRedirect: '/user/my/soundboards',
  failureRedirect: '/login',
  successFlash: 'Logged in successfully!',
  failureFlash: 'Invalid username or password.'
});

exports.register = function(req, res, next) {
  /* eslint-disable no-multi-spaces */
  var post     = req.body;
  // Usernames are forced to be lowercase in models/user.js.
  var username = post.username;
  var email    = post.email;
  var password = post.password;
  /* eslint-enable no-multi-spaces */

  req.checkBody('username', 'Invalid username.').notEmpty();
  req.checkBody('email', 'Invalid email.').notEmpty().isEmail();
  req.checkBody('password', 'Invalid password.').notEmpty();
  req.checkBody('password', 'Password is too short (minimum of 8 characters).').
    len(8);
  req.checkBody('password2', 'Password mismatch.').equals(password);

  var errors = req.validationErrors();
  if (errors) {
    req.flash('error', errors[0].msg);
    return res.redirect('/login');
  }

  var userObj = new User({
    username: username,
    email: email
  });

  User.register(userObj, password, function(errRegister, user) {
    if (errRegister) {
      return next(errRegister);
    }
    req.login(user, function(errLogin) {
      if (errLogin) {
        return next(errLogin);
      }

      req.flash('success', 'Registered successfully!');
      return res.redirect('/');
    });
  });
};

exports.soundboards = function (req, res, next) {
  var id = req.params.id;
  User.load(id, function (err, user) {
    if (err) {
      return next(err);
    }
    return res.render('user_soundboards', {
      soundboards: user.soundboards,
      user: user
    });
  });
};

exports.self = function (req, res) {
  return res.redirect('/user/' + req.user._id);
};
