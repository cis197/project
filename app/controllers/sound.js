var db = require('../../config/db');

var Soundboard = db.model('Soundboard'),
    Sound = db.model('Sound'),
    File = db.model('File');

exports.get = function(req, res, next) {
  var id = req.params.id;
  Sound.load(id, function (errSound, sound) {
    if (errSound) {
      return next(errSound);
    }
    File.load(sound.fid, function (errFile, file) {
      if (errFile) {
        return next(errFile);
      }
      res.type('mp3');
      res.send(file.buffer);
    });
  });
};

exports.create = function(req, res, next) {
  var post = req.body;
  var user = req.user;
  var name = post.name;
  var boardId = post.board;

  var backtrack = '/soundboard/' + boardId + '/edit';

  req.checkBody('name', 'Invalid name.').notEmpty();
  req.checkBody('name', 'Name cannot exceed 100 characters.').len(1, 100);

  var errors = req.validationErrors();
  if (errors) {
    req.flash('error', errors[0].msg);
    return res.redirect(backtrack);
  }

  Soundboard.load(boardId, function (errBoard, board) {
    if (errBoard) {
      return next(errBoard);
    }
    // Someone is trying to add a sound to a board that is not theirs.
    if (board.user.toString() !== user._id.toString()) {
      console.log(user._id.toString() + ' tried to add a sound to a board ' +
        'owned by ' + board.user.toString() + '.');
      req.flash('error', 'You do not own that soundboard!');
      return res.redirect('/');
    }

    // Otherwise, we know that this user owns the soundboard, and we can thus
    // attempt to add the sound.
    var file = req.files.file;
    if (!file) {
      req.flash('error', 'You did not include a file!');
      return res.redirect(backtrack);
    }
    if (file.extension.toLowerCase() !== 'mp3') {
      req.flash('error', 'File is not an MP3!');
      return res.redirect(backtrack);
    }
    if (file.size >= 100 * 1000) {
      req.flash('error', 'File is too large!');
      return res.redirect(backtrack);
    }

    // Soundboards can only have a maximum of 100 sounds.
    if (board.sounds.length >= 100) {
      req.flash('error', 'Soundboards can only have 100 sounds on them.');
      return res.redirect(backtrack);
    }

    var fileObj = new File({
      buffer: file.buffer
    });
    fileObj.save(function (errFile, fileRes) {
      if (errFile) {
        return next(errFile);
      }
      var soundObj = new Sound({
        name: name,
        fid: fileRes._id,
        user: user._id,
        soundboard: board._id
      });
      soundObj.save(function (errSound, sound) {
        if (errSound) {
          return next(errSound);
        }
        // Finally, update this reference on the soundboard.
        board.sounds.push(sound);
        board.save(function (errBoard2) {
          if (errBoard2) {
            return next(errBoard2);
          }
          req.flash('success', 'Added "' + name + '" to your soundboard.');
          return res.redirect(backtrack);
        });
      });
    });
  });
};

exports.delete = function (req, res, next) {
  var id = req.params.id;
  var user = req.user;

  Sound.load(id, function (errSound, sound) {
    if (errSound) {
      return next(errSound);
    }
    if (sound.user.toString() !== user._id.toString()) {
      console.log(user.toString() + ' tried to delete a sound that was ' +
        'owned by ' + sound.user.toString() + '.');
      req.flash('error', 'You do not own that sound!');
      return res.redirect('/');
    }

    // Delete the file.
    var fileId = sound.fid;
    var boardId = sound.soundboard;
    File.findByIdAndRemove(fileId, function (errFile) {
      if (errFile) {
        return next(errFile);
      }
      // Next, remove this sound from the soundboard.
      Soundboard.load(boardId, function (errBoard, board) {
        if (errBoard) {
          return next(errBoard);
        }
        var i = board.sounds.indexOf(sound);
        board.sounds.splice(i, i);
        board.save(function (errBoard2) {
          if (errBoard2) {
            return next(errBoard2);
          }
          // Finally, delete the sound itself.
          Sound.findByIdAndRemove(id, function (errSound2) {
            if (errSound2) {
              return next(errSound2);
            }
            return res.redirect('/soundboard/' + boardId + '/edit');
          });
        });
      });
    });
  });
};
